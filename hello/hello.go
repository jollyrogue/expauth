package hello

import (
	"log"
	"net/http"
	"text/template"
)

var tplHelloGet *template.Template
var tplHelloPost *template.Template

type HelloData struct {
	Name string
}

func init() {
	tplHelloGet = template.Must(template.New("index.tmpl").ParseFiles("templates/index.tmpl"))
	tplHelloPost = template.Must(template.New("formdata.tmpl").ParseFiles("templates/formdata.tmpl"))
}

func HelloGet(w http.ResponseWriter, r *http.Request) {
	if err := tplHelloGet.Execute(w, ""); err != nil {
		log.Printf("ERROR: (HelloGet) Problem with template. %s", err)
		http.Error(
			w,
			http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError,
		)
		return
	}
}

func HelloPost(w http.ResponseWriter, r *http.Request) {
	var data HelloData

	log.Println("Parsing form...")
	if err := r.ParseForm(); err != nil {
		log.Printf("ERROR: (HelloPost) Problem parsing form. %s", err)
		http.Error(
			w,
			http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError,
		)
		return
	}

	log.Printf("Form Values: %+v", r.Form)

	data.Name = r.PostFormValue("name")

	if err := tplHelloPost.Execute(w, data); err != nil {
		log.Printf("ERROR: (HelloPost) Problem with template. %s", err)
		http.Error(
			w,
			http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError,
		)
		return
	}
}
