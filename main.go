package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/jollyrogue/expauth/hello"

	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("Authentication Experiment Starting...")

	router := mux.NewRouter()
	router.HandleFunc("/", hello.HelloGet).Methods("GET")
	router.HandleFunc("/", hello.HelloPost).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", router))
}
